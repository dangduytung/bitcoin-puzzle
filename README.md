## Description

This project is dedicated to solving the Bitcoin Puzzle challenge. The challenge involves attempting to solve a puzzle related to Bitcoin transactions by generate random BTC wallet and compare with target BTC address. You can find more details about the puzzle and how to participate [here](https://privatekeys.pw/puzzles/bitcoin-puzzle-tx).


## Installation

```bash
$ pnpm install
```

## Running the app

```bash
# development
$ pnpm run start

# watch mode
$ pnpm run start:dev

# production mode
$ pnpm run start:prod
```

## Test

```bash
# unit tests
$ pnpm run test

# unit test one
$ pnpm test -- --testNamePattern='getRandomKeyInBits function'

# e2e tests
$ pnpm run test:e2e

# test coverage
$ pnpm run test:cov
```

## License

Nest is [MIT licensed](LICENSE).
