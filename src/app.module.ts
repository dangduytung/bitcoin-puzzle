import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { BitcoinModule } from './bitcoin/bitcoin.module';
import { StartupModule } from './startup.module';
import { TelegramModule } from './telegram/telegram.module';

@Module({
  imports: [
    ConfigModule.forRoot(),
    BitcoinModule,
    TelegramModule,
    StartupModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
