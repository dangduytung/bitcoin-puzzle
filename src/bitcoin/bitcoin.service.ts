import { Injectable } from '@nestjs/common';
import * as BIP39 from 'bip39';
import * as BTC from 'bitcoinjs-lib';
import { ECPairFactory } from 'ecpair';
import * as TINYSECP from 'tiny-secp256k1';

@Injectable()
export class BitcoinService {
  ECPair: any;
  MAIN_NET: boolean;
  NETWORK: any;

  constructor() {
    this.ECPair = ECPairFactory(TINYSECP);
    this.MAIN_NET = true;
    this.NETWORK = this.MAIN_NET ? BTC.networks.bitcoin : BTC.networks.testnet;
  }

  async getBtcFromPrivateKey(privateKey: string): Promise<any> {
    const mnemonic = BIP39.entropyToMnemonic(privateKey);
    const buffer = Buffer.from(privateKey, 'hex');

    /* Compressed */
    const keyPairC = this.ECPair.fromPrivateKey(buffer, {
      network: this.NETWORK,
    });
    const wifC = keyPairC.toWIF();
    const publicKeyC = keyPairC.publicKey.toString('hex');
    const addressC = BTC.payments.p2pkh({
      pubkey: keyPairC.publicKey,
      network: this.NETWORK,
    }).address;

    /* Uncompressed */
    const keyPairU = this.ECPair.fromPrivateKey(buffer, {
      compressed: false,
      network: this.NETWORK,
    });
    const wifU = keyPairU.toWIF();
    const publicKeyU = keyPairU.publicKey.toString('hex');
    const addressU = BTC.payments.p2pkh({
      pubkey: keyPairU.publicKey,
      network: this.NETWORK,
    }).address;

    const btc = {
      mnemonic,
      privateKey,
      wifC,
      publicKeyC,
      addressC,
      wifU,
      publicKeyU,
      addressU,
    };
    // console.log('btc', btc);
    return btc;
  }
}
