import { Module } from '@nestjs/common';
import { BitcoinService } from './bitcoin/bitcoin.service';
import { StartupService } from './startup.service';
import { TelegramService } from './telegram/telegram.service';

@Module({
  providers: [StartupService, BitcoinService, TelegramService],
})
export class StartupModule {}
