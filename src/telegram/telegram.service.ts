import { Injectable, Logger } from '@nestjs/common';
import * as TelegramBot from 'node-telegram-bot-api';

@Injectable()
export class TelegramService {
  private bot: TelegramBot;
  private readonly logger = new Logger(TelegramService.name);

  constructor() {
    this.initializeBot();
  }

  private async initializeBot(): Promise<void> {
    try {
      if (process.env.TELEGRAM_BOT_TOKEN && process.env.TELEGRAM_CHAT_ID) {
        this.bot = new TelegramBot(process.env.TELEGRAM_BOT_TOKEN);
        this.setupListeners();
      } else {
        throw new Error('Telegram bot token or chat ID is missing');
      }
    } catch (error) {
      this.logger.error(`Error initializing Telegram bot: ${error.message}`);
    }
  }

  private setupListeners(): void {
    if (this.bot) {
      this.bot.on('polling_error', (error) => {
        this.logger.error(`Polling error: ${error.message}`);
      });

      this.bot.on('webhook_error', (error) => {
        this.logger.error(`Webhook error: ${error.message}`);
      });
    }
  }

  async sendMessage(message: string): Promise<void> {
    if (!this.bot) {
      this.logger.error('Telegram bot is not initialized');
      return;
    }
    const options: TelegramBot.SendMessageOptions = {
      parse_mode: 'HTML', // optional, if you want to send formatted text
      disable_notification: false, // enable notifications
    };
    try {
      await this.bot.sendMessage(
        process.env.TELEGRAM_CHAT_ID,
        message,
        options,
      );
      this.logger.log('Message sent successfully');
    } catch (error) {
      this.logger.error(`Error sending message: ${error.message}`);
    }
  }
}
