import { Test, TestingModule } from '@nestjs/testing';
import { BitcoinService } from './bitcoin/bitcoin.service';
import { StartupService } from './startup.service';
import { TelegramService } from './telegram/telegram.service';

describe('StartupService', () => {
  let service: StartupService;
  let bitcoinService: BitcoinService;

  beforeEach(async () => {
    process.env.TELEGRAM_BOT_TOKEN = 'your_bot_token';
    process.env.TELEGRAM_CHAT_ID = 'your_chat_id';

    const module: TestingModule = await Test.createTestingModule({
      providers: [
        StartupService,
        BitcoinService,
        TelegramService,
        {
          provide: 'TELEGRAM_BOT_TOKEN',
          useValue: process.env.TELEGRAM_BOT_TOKEN,
        },
        {
          provide: 'TELEGRAM_CHAT_ID',
          useValue: process.env.TELEGRAM_CHAT_ID,
        },
      ],
    }).compile();

    service = module.get<StartupService>(StartupService);
    bitcoinService = module.get<BitcoinService>(BitcoinService);
  });

  afterEach(() => {
    jest.clearAllMocks(); // Clear all mocks after each test
  });

  it('should run infinite loop', async () => {
    // Source: https://privatekeys.pw/puzzles/bitcoin-puzzle-tx
    process.env.BITS = '65'; // Mock the necessary environment variables
    process.env.BITCOIN_ADDRESS = '18ZMbwUFLMHoZBbfpCjUJQTCMCbktshgpe'; // Mock the necessary environment variables

    // Mock BitcoinService methods
    const mockBitcoinData = {
      addressC: '18ZMbwUFLMHoZBbfpCjUJQTCMCbktshgpe',
      privateKey:
        '000000000000000000000000000000000000000000000001a838b13505b26867',
    };
    const getBtcFromPrivateKeySpy = jest
      .spyOn(bitcoinService, 'getBtcFromPrivateKey')
      .mockResolvedValue(mockBitcoinData);

    // Call the method to test
    await service.solveBitcoinPuzzle();

    // Assertions
    expect(getBtcFromPrivateKeySpy).toHaveBeenCalled();
  });
});
