import { randomBytes } from 'crypto';

const PRIVATE_KEY_LENGTH = 64;

export function getRandomNumberInRange(start: bigint, stop: bigint): bigint {
  if (start >= stop) {
    return start;
  }

  const range = stop - start;
  const bitsNeeded = range.toString(2).length;
  const bytesNeeded = Math.ceil(bitsNeeded / 8);

  while (true) {
    const randomBigInt = getBigIntFromRandomBytes(bytesNeeded);
    if (randomBigInt < range) {
      return start + randomBigInt;
    }
  }
}

function getBigIntFromRandomBytes(bytes: number): bigint {
  const randomBytesBuffer = randomBytes(bytes);
  return BigInt(`0x${randomBytesBuffer.toString('hex')}`);
}

export function generateHexRange(bits: number): { from: bigint; to: bigint } {
  const from = BigInt(2) ** BigInt(bits - 1);
  const to = BigInt(2) ** BigInt(bits) - BigInt(1);
  return { from, to };
}

export function getRandomKeyInBits(bits: number): string {
  const { from, to } = generateHexRange(bits);
  const randomKey = getRandomNumberInRange(from, to);
  return randomKey.toString(16).padStart(PRIVATE_KEY_LENGTH, '0');
}
