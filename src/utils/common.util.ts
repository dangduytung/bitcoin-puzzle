import BigNumber from 'bignumber.js';

const PRIVATE_KEY_LENGTH = 64;

export function getRandomNumber(min: number, max: number): number {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

export function powerOfTwoToHex(power: number, offset: number = 0): string {
  const value = new BigNumber(2).pow(power).plus(offset);
  const hexString = value.toString(16);
  return hexString;
}

export function randomHexaString(length: number): string {
  const characters = '0123456789abcdef';
  const result: string[] = [];
  for (let i = 0; i < length; i++) {
    result.push(
      characters.charAt(Math.floor(Math.random() * characters.length)),
    );
  }
  return result.join('');
}

export function randomHexBitsPrivateKeyRange(power: number): string {
  const min = new BigNumber(2).pow(power - 1);
  const max = new BigNumber(2).pow(power).minus(1);
  const minHex = min.toString(16);
  const maxHex = max.toString(16);
  const firstChar = getRandomNumber(
    parseInt(minHex.charAt(0), 16),
    parseInt(maxHex.charAt(0), 16),
  );
  const lastChar = randomHexaString(minHex.length - 1);
  const randomHex = firstChar + lastChar;
  return randomHex;
}

export function randomPrivateKey(power: number): string {
  const randomHexEnd = randomHexBitsPrivateKeyRange(power);
  const firstLength = PRIVATE_KEY_LENGTH - randomHexEnd.length;
  return '0'.repeat(firstLength) + randomHexEnd;
}

export async function delay(ms: number) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}
