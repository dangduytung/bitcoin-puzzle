import {
  generateHexRange,
  getRandomKeyInBits,
  getRandomNumberInRange,
} from './bitcoin.util';

/**
 * https://privatekeys.pw/puzzles/bitcoin-puzzle-tx
 */

describe('getRandomNumberInRange function', () => {
  it('should generate a random number within the specified range', () => {
    const start =
      '0000000000000000000000000000000000000000000000000000000000000000';
    const stop =
      'ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff';

    const result = getRandomNumberInRange(start, stop);

    // Convert the result to a BigInt and check if it's within the range
    const randomNumber = BigInt(`0x${result}`);
    const startBigInt = BigInt(`0x${start}`);
    const stopBigInt = BigInt(`0x${stop}`);
    expect(randomNumber).toBeGreaterThanOrEqual(startBigInt);
    expect(randomNumber).toBeLessThan(stopBigInt);
  });
});

describe('getRandomNumberInRange with short range', () => {
  it('should generate random numbers within the specified range', () => {
    const start =
      'fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffe';
    const stop =
      'ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff';

    const result = getRandomNumberInRange(start, stop);
    console.log(`Result: ${result}`);
    const randomNumber = BigInt(`0x${result}`);
    const startBigInt = BigInt(`0x${start}`);
    const stopBigInt = BigInt(`0x${stop}`);
    expect(randomNumber).toBeGreaterThanOrEqual(startBigInt);
    expect(randomNumber).toBeLessThan(stopBigInt);
  });
});

describe('getRandomNumberInRange function with threshold check', () => {
  it('should generate a random number greater than the threshold', () => {
    const start =
      '0000000000000000000000000000000000000000000000020000000000000000';
    const stop =
      '000000000000000000000000000000000000000000000003ffffffffffffffff';
    const threshold =
      '0000000000000000000000000000000000000000000000030000000000000000';

    const totalIterations = 10; // Number of iterations
    let countThresholdMet = 0;

    // Loop multiple times and count how many times the threshold is met
    for (let i = 0; i < totalIterations; i++) {
      const result = getRandomNumberInRange(start, stop);
      //   console.log(`Iteration ${i + 1}: ${result}`);
      const randomNumber = BigInt(`0x${result}`);
      const thresholdBigInt = BigInt(`0x${threshold}`);
      if (randomNumber > thresholdBigInt) {
        countThresholdMet++;
      }
    }

    // Calculate percentage of results that meet the threshold
    const percentageThresholdMet = (countThresholdMet / totalIterations) * 100;

    // Output the percentage
    console.log(
      `Percentage of results greater than threshold: ${percentageThresholdMet}%`,
    );

    // Assert that at least some percentage of results meet the threshold (adjust as needed)
    expect(percentageThresholdMet).toBeGreaterThan(0);
  });
});

describe('generateHexRange function', () => {
  test('should return correct range for bits 1', () => {
    const range = generateHexRange(1);
    expect(range).toEqual({
      from: '0000000000000000000000000000000000000000000000000000000000000001',
      to: '0000000000000000000000000000000000000000000000000000000000000001',
    });
  });

  test('should return correct range for bits 2', () => {
    const range = generateHexRange(2);
    expect(range).toEqual({
      from: '0000000000000000000000000000000000000000000000000000000000000002',
      to: '0000000000000000000000000000000000000000000000000000000000000003',
    });
  });

  test('should return correct range for bits 66', () => {
    const range = generateHexRange(66);
    expect(range).toEqual({
      from: '0000000000000000000000000000000000000000000000020000000000000000',
      to: '000000000000000000000000000000000000000000000003ffffffffffffffff',
    });
  });
});

describe('getRandomKeyInBits function', () => {
  const bits = 66; // Example number of bits

  it('should generate a random key within the specified number of bits', () => {
    const range = generateHexRange(bits);

    const privateKey = getRandomKeyInBits(bits);

    console.log(`Private Key: ${privateKey}`);

    expect(typeof privateKey).toBe('string');
    expect(BigInt(`0x${privateKey}`)).toBeGreaterThanOrEqual(
      BigInt(`0x${range.from}`),
    );
    expect(BigInt(`0x${privateKey}`)).toBeLessThanOrEqual(
      BigInt(`0x${range.to}`),
    );
  });
});
