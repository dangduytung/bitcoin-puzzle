import { Injectable, Logger, OnApplicationBootstrap } from '@nestjs/common';
import * as fs from 'fs';
import { BitcoinService } from './bitcoin/bitcoin.service';
import { TelegramService } from './telegram/telegram.service';
import { randomPrivateKey } from './utils/common.util';
import { getRandomKeyInBits } from './utils/bitcoin.util';

@Injectable()
export class StartupService implements OnApplicationBootstrap {
  private readonly logger = new Logger(StartupService.name);
  private readonly TIME_DELAY: number;

  constructor(
    protected readonly bitcoinService: BitcoinService,
    protected readonly telegramService: TelegramService,
  ) {
    this.TIME_DELAY = parseInt(process.env.TIME_DELAY, 10);
    this.logger.log('========== STARTUP SERVICE =========');
    this.logger.log(
      `BITS: ${process.env.BITS}, BITCOIN_ADDRESS: ${process.env.BITCOIN_ADDRESS}`,
    );
    this.logger.log(`TIME_DELAY: ${this.TIME_DELAY} (ms)`);
    this.logger.log('====================================');
  }

  async onApplicationBootstrap() {
    this.solveBitcoinPuzzle();
  }

  async solveBitcoinPuzzle() {
    let counter = 0;
    const bits = parseInt(process.env.BITS, 10);

    while (true) {
      // const privateKey = randomPrivateKey(bits);
      const privateKey = getRandomKeyInBits(bits);

      const btc = await this.bitcoinService.getBtcFromPrivateKey(privateKey);
      // this.logger.log(btc);

      // Increment the counter
      counter++;

      // Log the counter value by step
      if (counter % 100000 === 0) {
        this.logger.log(
          `Counter: ${counter}, Private Key: ${privateKey}, Address: ${btc.addressC}`,
        );
      }

      // Check if Bitcoin address matches
      if (
        process.env.BITCOIN_ADDRESS.toLowerCase() === btc.addressC.toLowerCase()
      ) {
        // Write log BTC matches
        this.logger.log(btc);

        // Write to file
        const filePath = 'success.txt';
        try {
          await this.writeToFile(JSON.stringify(btc), filePath);
          this.logger.log('Data has been written to the file successfully.');
        } catch (error) {
          this.logger.error('Error writing to file:', error);
        }

        // Send message to Telegram
        const message = `BTC address: ${btc.addressC}\nBTC private key: ${btc.privateKey}\nBTC: ${JSON.stringify(btc)}`;
        await this.telegramService.sendMessage(message);

        // Exit the loop
        break;
      }

      // Delay
      await this.delayGenerateBtc();
    }
  }

  async delayGenerateBtc() {
    if (this.TIME_DELAY && this.TIME_DELAY > 0) {
      await this.delay(this.TIME_DELAY);
    }
  }

  async delay(ms: number) {
    return new Promise((resolve) => setTimeout(resolve, ms));
  }

  async writeToFile(data: string, filePath: string): Promise<void> {
    return new Promise<void>((resolve, reject) => {
      fs.writeFile(filePath, data, (error) => {
        if (error) {
          reject(error);
        } else {
          resolve();
        }
      });
    });
  }
}
